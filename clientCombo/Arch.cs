﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace combo
{
    public class Arch
    {
        public Node Origin { get; set; }
        public Node Destination { get; set; }

        public Arch(Node origin, Node destination)
        {
            Origin = origin;
            Destination = destination;
        }
    }
}
