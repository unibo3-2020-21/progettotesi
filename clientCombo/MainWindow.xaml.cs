﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections;
using System.Net.Http.Json;
using Newtonsoft.Json;
using System.Text.Json;
using System.Windows.Threading;

namespace combo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public enum state
        {
            INSERT,
            DELETE
        }
        private state status = state.INSERT;

        //http variables
        private static HttpClient client = new HttpClient();
        private static string baseUri = "api/graph/";

        //identifier: contains just one element that is the unique string set as id of the client (put also into the Node class to identify it)
        //CAN NOT use just a string because it needs to change
        private IList<string> identifier = new List<string>();      

        //graph is the logic structure of the graph saved on the server
        //graphElements is the visual structure of the graph that displays the logic one to the canvas correctly
        private Dictionary<NodeGraphics, List<Node>> graphElements = new Dictionary<NodeGraphics, List<Node>>();
        //private IGraph graph;

        //Graphic settings to paint the node
        private SettingsNode settings;
        private Brush stroke;
        private Brush fill;
        private int counter = 0;

        //teporary variables used to draw correctly nodes and arches
        #nullable enable
        private NodeGraphics? start;
        private NodeGraphics? end;
        private Line? actualLine;

        private DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();


        public MainWindow()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("https://localhost:44371"); //TODO put ngrok

            SetSettings();

            SetColors();
            
            SetIdentifier();

            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 30);
            dispatcherTimer.Start();

            DrawGraph();

        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            DrawGraph();
        }
        private void SetSettings()
        {
            HttpResponseMessage resp = client.GetAsync(baseUri + "getSettings").Result;
            resp.EnsureSuccessStatusCode();
            settings = resp.Content.ReadAsAsync<SettingsNode>().Result;
        }

        private void SetIdentifier()
        {
            identifier.Add("no");  //set memory for one element, otherwise exception
            bool established;
            do
            {
                string id = GetId();
                string send = string.Format(baseUri + "getOkIdentifier/{0}", id);
                Debug.WriteLine("send string: " + send);
                HttpResponseMessage resp = client.GetAsync(send).Result;
                resp.EnsureSuccessStatusCode();
                established = resp.Content.ReadAsAsync<bool>().Result;
                identifier[0] = id;

            } while (!established);
        }
        private static string GetId()
        {
            int size = 5;
            var builder = new StringBuilder(size);

            // Unicode/ASCII Letters are divided into two blocks
            // (Letters 65–90 / 97–122):
            // The first group containing the uppercase letters and
            // the second group containing the lowercase.  

            // char is a single Unicode character  
            char offset = 'A'; //change to 'a' for lowercase
            const int lettersOffset = 26; // A...Z or a..z: length=26  
            Random r = new Random();
            for (var i = 0; i < size; i++)
            {
                var @char = (char)r.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }
            return builder.ToString();
            //return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }

        private void DrawGraph()
        {
            ClearCanvasAndStructure();
            HttpResponseMessage respGraph = client.GetAsync(baseUri + "getGraph").Result;
            respGraph.EnsureSuccessStatusCode();
            string sGraph = respGraph.Content.ReadAsAsync<string>().Result;
            Dictionary<string, KeyValuePair<Node, List<Node>>> temp = JsonConvert.DeserializeObject<Dictionary<string, KeyValuePair<Node, List<Node>>>>(sGraph);

            foreach (var row in temp)
            {
                NodeGraphics nodeGr = createNodeGraphics(row.Value.Key);
                graphElements.Add(nodeGr, row.Value.Value);
            }


            /*ClearCanvasAndStructure();
            HttpResponseMessage respStruct = client.GetAsync(baseUri + "getGraphStructure").Result;
            respStruct.EnsureSuccessStatusCode();
            string serializedGraph = respStruct.Content.ReadAsAsync<string>().Result;
            List<KeyValuePair<Node, List<Node>>> graph1 = JsonConvert.DeserializeObject<List<KeyValuePair<Node, List<Node>>>>(serializedGraph);

            foreach(var node in graph1)
            {
                NodeGraphics nodeGr = createNodeGraphics(node.Key);
                graphElements.Add(nodeGr, node.Value);
                
            }*/
            
            //NB: se inverto le funzioni di disegno gli archi vengono disegnati sopra i nodi
            DrawArchesGraphics();
            DrawNodeGraphics();
        }

        private void ClearCanvasAndStructure()
        {
            canvas.Children.Clear();
            graphElements.Clear();  
        }

        private void DrawArchesGraphics()
        {
            foreach (NodeGraphics ngr in graphElements.Keys)
            {
                foreach (Node connection in graphElements[ngr])
                {
                    Point start = new Point(ngr.Base.Centre.Key, ngr.Base.Centre.Value);
                    Point end = new Point(connection.Centre.Key, connection.Centre.Value);

                    DrawArch(start, end);
                }
            }

        }


        private void DrawNodeGraphics()
        {
            foreach(NodeGraphics nodeGr in graphElements.Keys)
            {
                Canvas.SetLeft(nodeGr.Shape, nodeGr.Base.Centre.Key - (settings.Size / 2));
                Canvas.SetTop(nodeGr.Shape, nodeGr.Base.Centre.Value - (settings.Size / 2));

                canvas.Children.Add(nodeGr.Shape);
            }
            
        }

        private NodeGraphics createNodeGraphics(Node node)
        {
            Ellipse el = new Ellipse();
            el.Stroke = this.stroke;
            el.Fill = this.fill;
            el.Width = settings.Size;
            el.Height = settings.Size;
            return new NodeGraphics(node, el);
        }

        private void SetColors()
        {
            //#TODO check default not implemented
            switch (settings.Fill)
            {
                case "Red":
                    fill = Brushes.Red;
                    break;
                default:
                    break;
            }

            switch (settings.Stroke)
            {
                case "AliceBlue":
                    stroke = Brushes.AliceBlue;
                    break;
                default:
                    break;
            }
        }

        private void TouchPress(object sender, TouchEventArgs e)
        {
            Point touchPoint = e.GetTouchPoint(null).Position;

            Press(touchPoint);
        }

        private void Press(Point position)
        {
            switch (status)
            {
                case state.INSERT:
                    InsertTouchDown(canvas, position);
                    break;
                case state.DELETE:
                    DeleteTouchDown(position);
                    break;
            }
            DrawGraph();
        }

        private void DeleteTouchDown(Point touchPoint)
        {
            start = GetNodeGr(touchPoint);
        }

        private void InsertTouchDown(Canvas canv, Point pt)
        {
            Node node = new Node(identifier[0], counter);
            SetNode(node, pt);

            HttpResponseMessage resp1 = client.PostAsJsonAsync(baseUri + "postNode", node).Result;
            resp1.EnsureSuccessStatusCode();
            bool successfullPost = resp1.Content.ReadAsAsync<bool>().Result;

            if (successfullPost)
            {
                counter++;
            }
            else
            {
                start = GetNodeGr(pt);
            }

        }

        private NodeGraphics? GetNodeGr(Point pt)
        {
            foreach (NodeGraphics ngr in graphElements.Keys)
            {
                if (Math.Abs(ngr.Base.Centre.Key - pt.X) < (settings.Size / 2) && Math.Abs(ngr.Base.Centre.Value - pt.Y) < (settings.Size / 2))
                {
                    return ngr;
                }
            }
            return null;
        }

        private void SetNode(Node node, Point centre)
        {
            node.Fill = settings.Fill;
            node.Stroke = settings.Stroke;
            node.Radious = settings.Size;
            node.Centre = new KeyValuePair<double,double>(centre.X,centre.Y);
            node.connectedTo = new HashSet<Node>();
        }

        private void DrawArch(Point start, Point end)
        {
            Line _line = new Line();
            _line.Stroke = Brushes.White;
            _line.X1 = start.X - 1;
            _line.X2 = end.X - 1;
            _line.Y1 = start.Y - 1;
            _line.Y2 = end.Y - 1;
            _line.StrokeThickness = 2;
            this.canvas.Children.Add(_line);
        }

        private void DrawTemporaryArch(Point start, Point end)
        {
            Line _line = new Line();
            switch (status)
            {
                case state.INSERT:
                    _line.Stroke = Brushes.White;
                    break;
                case state.DELETE:
                    _line.Stroke = Brushes.Blue;
                    break;
                default:
                    _line.Stroke = Brushes.White;
                    break;
            }
            _line.X1 = start.X - 1;
            _line.X2 = end.X - 1;
            _line.Y1 = start.Y - 1;
            _line.Y2 = end.Y - 1;
            _line.StrokeThickness = 2;
            this.actualLine = _line;
            this.canvas.Children.Add(this.actualLine);
        }

        private void TouchMoving(object sender, TouchEventArgs e)
        {
            Moving(e.GetTouchPoint(null).Position);
        }

        private void Moving (Point position)
        {
            if (this.start != null)
            {
                try
                {
                    this.RemoveActualLine();
                }
                catch (NullReferenceException)
                {
                    Debug.WriteLine("null exception!");
                }
                Point beginning = new Point(start.Base.Centre.Key, start.Base.Centre.Value);
                DrawTemporaryArch(beginning, position);
            }
        }

        private void TouchRelease(object sender, TouchEventArgs e)
        {
            Release(e.GetTouchPoint(null).Position);
        }

        private void Release(Point position)
        {
            if (this.start != null)
            {
                this.end = GetNodeGr(position);
                switch (status)
                {
                    case state.INSERT:
                        InsertTouchRelease();
                        break;
                    case state.DELETE:
                        DeleteTouchRelease();
                        break;


                }
            }
            RemoveActualLine();
            DrawGraph();
            ClearForNewArch();
        }

        private void DeleteTouchRelease()
        {
            if(this.end != null)
            {
                if (start != null && start.Base.Equals(end.Base))
                {
                    HttpResponseMessage respDeleteNode = client.PostAsJsonAsync(baseUri + "postDeleteNode", start.Base).Result;
                    respDeleteNode.EnsureSuccessStatusCode();
                }
                else
                {
                    GeneralPostArch("postDeleteArch", new Arch(start.Base, this.end.Base));
                }
            }
        }

        private void InsertTouchRelease()
        {
            if (end != null && start != null && !start.Base.Equals(end.Base))
            {
                Arch arch = new Arch(this.start.Base, this.end.Base);
                GeneralPostArch("postArch", arch);
            }
        }

        private bool GeneralPostArch(string functionName, Arch arch)
        {
            HttpResponseMessage resp1 = client.PostAsJsonAsync(baseUri + functionName, arch).Result;
            resp1.EnsureSuccessStatusCode();
            return resp1.Content.ReadAsAsync<bool>().Result;
        }

        private void RemoveActualLine()
        {
            this.canvas.Children.Remove(this.actualLine);
        }

        private void ClearForNewArch()
        {
            this.start = null;
            this.end = null;
            this.actualLine = null;
        }

        private void DeleteStatus(object sender, TouchEventArgs e)
        {
            delete.Background = Brushes.Green;
            insert.Background = Brushes.LightSlateGray;
            status = state.DELETE;
        }

        private void InsertStatus(object sender, TouchEventArgs e)
        {
            insert.Background = Brushes.Green;
            delete.Background = Brushes.LightSlateGray;
            status = state.INSERT;
        }

        private void DeleteStatus(object sender, RoutedEventArgs e)
        {
            delete.Background = Brushes.Green;
            insert.Background = Brushes.LightSlateGray;
            status = state.DELETE;
        }

        private void InsertStatus(object sender, RoutedEventArgs e)
        {
            insert.Background = Brushes.Green;
            delete.Background = Brushes.LightSlateGray;
            status = state.INSERT;
        }

        private void MousePress(object sender, MouseButtonEventArgs e)
        {
            Press(e.GetPosition(null));
        }

        private void MouseMoving(object sender, MouseEventArgs e)
        {
            Moving(e.GetPosition(null));
        }

        private void MouseRelease(object sender, MouseButtonEventArgs e)
        {
            Release(e.GetPosition(null));

        }
    }
}