﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace combo
{
    public class Node : IEquatable<Node>
    {
        public string Fill { get; set; }
        public string Stroke { get; set; }
        public double Radious { get; set; }
        public KeyValuePair<double, double> Centre { get; set; }
        public int Value { get; set; }
        public string Label { get; set; }
        public HashSet<Node> connectedTo { get; set; }

        public Node() { }

        public Node(string label, int value)
        {
            Label = label;
            Value = value;
        }

        public override string ToString()
        {
            return Label + Value;
        }

        public bool Equals(Node other)
        {
            return (this.Label.Equals(other.Label) && this.Value == other.Value);
        }

        public override int GetHashCode()
        {
            return Label.GetHashCode() ^ Value;
        }
    }
}