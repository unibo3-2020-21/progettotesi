﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace severWeb.Models
{
    public interface IGraph
    {
        void AddNode(Node node);
        void RemoveNode(List<Node> indexes, Node node);
        void AddConnection(Node origin, Node destination);
        void RemoveConnection(Node origin, Node destination);

        bool IsNodePresent(Node node);

        bool IsNodePresent(KeyValuePair<double,double> position);

        Node GetNode(KeyValuePair<double, double> position);


        bool IsArchPresent(Node origin, Node destination);

        HashSet<Node> GetConnections(Node target);

        Dictionary<Node, HashSet<Node>> GetGraphStructure();
    }
}
