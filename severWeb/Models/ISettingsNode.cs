﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace severWeb.Models
{
    public interface ISettingsNode
    {
        string Fill { get; set; }

        string Stroke { get; set; }

        double Size { get; set; }

        
    }
}
