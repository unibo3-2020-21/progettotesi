﻿using Newtonsoft.Json;
using severWeb.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace severWeb.Controllers
{
    public class GraphController : ApiController
    {
        private ISettings settings = Settings.GetInstance();
        private IGraph graph = Graph.GetInstance();

        [HttpPost]
        [ActionName("postDeleteNode")]
        public bool PostDeleteNode(Node target)
        {
            if (graph.IsNodePresent(target))
            {
                List<Node> indexes = new List<Node>();
                var tempGraph = graph.GetGraphStructure();
                foreach (Node n in tempGraph.Keys)
                {
                    if (tempGraph[n].Contains(target))
                    {
                        indexes.Add(n);
                    }
                }
                graph.RemoveNode(indexes, target);
                return true;
            }
            return false;
        }

        [HttpPost]
        [ActionName("postDeleteArch")]
        public bool PostDeleteArch(Arch arch)
        {
            if (graph.IsArchPresent(arch.Origin, arch.Destination))
            {
                graph.RemoveConnection(arch.Origin, arch.Destination);
                return true;
            }
            return false;
        }

        [HttpPost]
        [ActionName("postNode")]
        public bool PostNode(Node node)
        {
            if (!graph.IsNodePresent(node))
            {
                graph.AddNode(node);
                return true;
            }
            return false;
        }

        [HttpPost]
        [ActionName("postArch")]
        public bool PostArch(Arch arch)
        {
            if (!graph.IsArchPresent(arch.Origin, arch.Destination))
            {
                graph.AddConnection(arch.Origin, arch.Destination);
                return true;
            }
            return false;
        }

        [HttpPost]
        [ActionName("postSettings")]
        public void PostSettings(SettingsNode settings)
        {
            this.settings.SettingsNode = settings;
        }

        [HttpGet]
        [ActionName("getGraph")]
        public string GetGraph()
        {
            Dictionary<string,KeyValuePair<Node, List < Node >>> tempGraph = new Dictionary<string, KeyValuePair<Node,List<Node>>>();
            foreach(var row in graph.GetGraphStructure())
            {
                tempGraph.Add(row.ToString(), new KeyValuePair<Node,List<Node>>(row.Key,row.Value.ToList()));
            }
            return JsonConvert.SerializeObject(tempGraph, Formatting.Indented); 
        }

        /*[HttpGet]
        [ActionName("getGraphStructure")]
        public string GetGraphStructure()
        {
            List<KeyValuePair<Node,List<Node>>> tempGraph = new List<KeyValuePair<Node, List<Node>>>();
            foreach(Node n in graph.GetGraphStructure().Keys)
            {
                tempGraph.Add(new KeyValuePair<Node, List<Node>>(n, graph.GetConnections(n).ToList()));
            }
            return JsonConvert.SerializeObject(tempGraph);
        }

        [HttpGet]
        [ActionName("getNodes")]
        public string GetNodes()
        {
            return System.Text.Json.JsonSerializer.Serialize(graph.GetGraphStructure().Keys.ToList());
        }

        [HttpPost]
        [ActionName("getConnections")]
        public string GetConnections(Node target)
        {
            return System.Text.Json.JsonSerializer.Serialize(graph.GetConnections(target).ToList());
        }*/

        [HttpGet]
        [ActionName("getSettings")]
        public ISettingsNode GetSettings()
        {
            return settings.SettingsNode;
        }


        [HttpGet]
        [ActionName("getOkIdentifier")]
        public bool AddIdentifier(string id)
        {
            if (!settings.ContainsClientId(id))
            {
                settings.AddClientId(id);
                return true;
            }
            return false;
            
        }

    }
}